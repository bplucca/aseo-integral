from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [
    path('',views.index,name='index'),
    path('registro',views.regform,name='registro'),
    path('login',views.signin,name='login'),
    path('reg_colab',views.reg_colab,name='reg_colab'),
    path('list_colab',views.ColabList.as_view(),name='list_colab'),
    url(r'^eliminarcolab/(?P<id_colaborador>\d+)/$', views.delete_colab, name='delete_colab'),
    path('list_empresa',views.EmpresaList.as_view(),name='list_empresa'),
    url(r'^eliminarempresa/(?P<id_empresa>\d+)/$', views.delete_empresa, name='delete_empresa'),
    path('list_solicitud',views.SolicitudList.as_view(),name='list_solicitud'),
    path('reg_empresa',views.reg_empresa,name='reg_empresa'),
    path('reg_turno',views.reg_turno, name='reg_turno'),
    path('reg_solicitud',views.reg_solicitud,name='reg_solicitud'),
    url(r'^editar/(?P<id_solicitud>\d+)/$',views.edit_solicitud, name='edit_solicitud'),
    path('done',views.done,name='done'),
]