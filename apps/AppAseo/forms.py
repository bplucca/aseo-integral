from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from apps.AppAseo.models import Colaborador, Empresa, Turno, Solicitud

class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', )

class ColabForm(forms.ModelForm):

    class Meta:
        model = Colaborador
        fields = [
            'nombre',
            'apellido',
            'rut',
        ]

class EmpresaForm(forms.ModelForm):

    class Meta:
        model = Empresa
        fields = [
            'nombre',
            'fono',
            'direc',
        ]

class TurnoForm(forms.ModelForm):

    class Meta:
        model = Turno
        fields = [
            'colaborador',
            'empresa',
            'fecha_inicio',
            'fecha_termino',
        ]
        widgets = {
            'colaborador': forms.Select(attrs={'class':'form-control'}),
            'empresa': forms.Select(attrs={'class':'form-control'}),
            'fecha_inicio': forms.DateInput(attrs={'class':'form-control'}),
            'fecha_termino': forms.DateInput(attrs={'class':'form-control','id':'datepicker'}),
        }

class SolicitudForm(forms.ModelForm):

    class Meta:
        model = Solicitud
        fields = [
            'colaborador',
            'tipo',
        ]
        widgets = {
            'colaborador': forms.Select(attrs={'class':'form-control'}),
            'tipo': forms.Select(attrs={'class':'form-control'}),
        }

class EstadoSolicitudForm(forms.ModelForm):

    class Meta:
        model = Solicitud
        fields = [
            'estado'
        ]
        widgets = {
            'estado': forms.Select(attrs={'class':'form-control'}),
        }
