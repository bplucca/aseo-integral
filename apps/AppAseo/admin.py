from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Colaborador)
admin.site.register(Empresa)
admin.site.register(Turno)
admin.site.register(Solicitud)