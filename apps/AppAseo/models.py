from django.db import models

# Create your models here.
class Colaborador(models.Model):
    nombre = models.CharField(max_length=25)
    apellido = models.CharField(max_length=15)
    rut = models.CharField(max_length=10)

    #pylint: disable=E1101
    def __str__(self):
        return self.nombre

class Empresa(models.Model):
    nombre = models.CharField(max_length=25)
    fono = models.CharField(max_length=15)
    direc = models.CharField(max_length=50, null=True, blank=True)

    #pylint: disable=E1101
    def __str__(self):
        return self.nombre

class Turno(models.Model):
    colaborador = models.ForeignKey(Colaborador, null=True, blank=True, on_delete=models.CASCADE)
    empresa = models.ForeignKey(Empresa, null=True, blank=True, on_delete=models.CASCADE)
    fecha_inicio = models.DateField(null=True, blank=True)
    fecha_termino = models.DateField(null=True, blank=True)

class Solicitud(models.Model):
    colaborador = models.ForeignKey(Colaborador,null=True, blank=True, on_delete=models.CASCADE)
    TIPOS = (('ENTRADA','ENTRADA'),
                ('SALIDA','SALIDA'),)
    tipo = models.CharField(max_length=10, choices = TIPOS, null=True, blank=True)
    fecha = models.DateTimeField(null=True, blank=True)
    ESTADO = (('ACEPTAR','ACEPTAR'),
                ('RECHAZAR','RECHAZAR'),)
    estado = models.CharField(max_length=10, choices = ESTADO, null=True, blank=True)


class Cuadrilla(models.Model):
    empresa = models.ForeignKey(Empresa,null=True, blank=True, on_delete=models.CASCADE)
    TIPOS = (('LIMPIEZA','LIMPIEZA'),
                ('CAFETERÍA','CAFETERÍA'),)
    tipo = models.CharField(max_length=10, choices = TIPOS, null=True, blank=True)