from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from apps.AppAseo.forms import *

from django.views.generic import ListView

# Create your views here.
def index(request):
    return render(request, 'AppAseo/index.html')

def regform(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)

        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('index')

    else:
        form = UserCreationForm()

    context = {'form' : form}

    return render(request, 'AppAseo/regform.html', context)

def signin(request):
    if request.method == "POST":
        form = AuthenticationForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('index')

    else:
        form = AuthenticationForm()

    context = {'form' : form}

    return render(request, 'AppAseo/login.html', context)

def reg_colab(request):
    if request.method == 'POST':
        form = ColabForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
        return redirect('list_colab')
    else:
        form = ColabForm()
    
    return render(request, 'AppAseo/reg_colab.html', {'form':form})

def delete_colab(request, id_colaborador):
    colaborador = Colaborador.objects.get(id=id_colaborador)
    if request.method == 'POST':
        colaborador.delete()
        return redirect('list_colab')
    return render(request,'AppAseo/delete_colab.html',{'colaborador':colaborador})

class ColabList(ListView):
    model = Colaborador
    template_name = 'AppAseo/list_colab.html'

def reg_empresa(request):
    if request.method == 'POST':
        form = EmpresaForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
        return redirect('list_empresa')
    else:
        form = EmpresaForm()
    
    return render(request, 'AppAseo/reg_empresa.html', {'form':form})

def delete_empresa(request, id_empresa):
    empresa = Empresa.objects.get(id=id_empresa)
    if request.method == 'POST':
        empresa.delete()
        return redirect('list_empresa')
    return render(request,'AppAseo/delete_empresa.html',{'empresa':empresa})

class EmpresaList(ListView):
    model = Empresa
    template_name = 'AppAseo/list_empresa.html'

def reg_turno(request):
    if request.method == "POST":
        form = TurnoForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('done')
    else:
        form = TurnoForm()
    
    return render(request,'AppAseo/reg_turno.html',{'form':form})

def reg_solicitud(request):
    if request.method == "POST":
        form = SolicitudForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('index')
    else:
        form = SolicitudForm()
    
    return render(request,'AppAseo/reg_solicitud.html',{'form':form})

def edit_solicitud(request, id_solicitud):
    solicitud = Solicitud.objects.get(id = id_solicitud)
    if request.method == 'GET':
        form = EstadoSolicitudForm(instance=solicitud)
    else:
        form = EstadoSolicitudForm(request.POST, instance=solicitud)
        if form.is_valid():
            form.save()
        return redirect('list_solicitud')
    return render(request,'AppAseo/edit_solicitud.html',{'form':form})

class SolicitudList(ListView):
    model = Solicitud
    template_name = 'AppAseo/list_solicitud.html'

def done(request):
    return render(request, 'AppAseo/done.html')