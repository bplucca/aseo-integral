# Generated by Django 2.0.9 on 2018-12-07 06:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('AppAseo', '0007_auto_20181207_0348'),
    ]

    operations = [
        migrations.AlterField(
            model_name='solicitud',
            name='tipo',
            field=models.CharField(blank=True, max_length=10, null=True),
        ),
    ]
